#!/bin/bash

log() {
    echo "[Sitepilot] $1"
}

echo ""
echo ""
echo "***************************************************"
echo "* Sitepilot PHP Container"
echo "* "
echo "* Environment:"
echo "* - SP_APP_NAME: $SP_APP_NAME"
echo "* - SP_UPLOAD_SIZE: $SP_UPLOAD_SIZE"
echo "* - SP_WP_INSTALL: $SP_WP_INSTALL"
echo "*"
echo "* Support: https://sitepilot.io"
echo "***************************************************"
echo ""
echo ""


log "Creating default directories..."
mkdir -p /var/www/log
mkdir -p /var/www/htdocs

log "Generating configuration based on environment..."
cat /opt/sitepilot/templates/main.tmpl | perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg' > /usr/local/etc/php/conf.d/main.ini

if [ "$SP_WP_INSTALL" = true ]; then
    if [ -n "$(find "/var/www/htdocs" -maxdepth 0 -type d -empty 2>/dev/null)" ]; then
        log "Installing WordPress in /var/www/htdocs..."
        wp core download --path="/var/www/htdocs" --allow-root
    else
        log "WordPress already installed, skipping installation..."
    fi
fi

log "Resetting file permissions to www-data..."
chown -R www-data:www-data /var/www/htdocs

log "Starting sshd..."

/usr/sbin/sshd
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start sshd: $status"
  exit $status
fi

log "Starting php..."

set -euo pipefail
exec "$@"
